#!/bin/bash

# Bootstrap - *local* provisioning for sandel-dev machines
# (using chef-zero instead of Chef Server)
#
# In order to successfully bootstrap, the following minimums are required:
#
# Linux (RHEL)
#   1. yum installed
#   2. subscription-manager registered and attached to a valid Red Hat
#      subscription
# 
# Linux (Debian/Ubuntu):
#   1. apt-get installed
#
# OS X:
#   1. brew (homebrew) installed - see http://brew.sh
#
# Windows: TBD
#

DEVOPS_PREFIX=/usr/local
DEVOPS_DIR=devops
CHEF_REPO=sandel-chef-repo
REMOTE_URL=https://jenkinz@bitbucket.org/jenkinz/$CHEF_REPO.git

LINUX=0
DARWIN=0
UNSUPPORTED=0

echo [INFO] Bootstrapping DevOps provisioning for sandel-dev machine...

# Detect Platform

case `uname` in
    Linux )
        LINUX=1
        echo [INFO] Linux detected
        which yum > /dev/null 2>&1 && { DISTRO=1; }
        which apt-get > /dev/null 2>&1 && { DISTRO=2; }
        ;;
    Darwin )
        DARWIN=1
        echo [INFO] OS X detected
        ;;
    * )
        UNSUPPORTED=1
        echo [ERROR] OS unsupported, aborting
        exit 1
        ;;
esac

# Install/Update Git

git --version > /dev/null 2>&1
GIT_IS_INSTALLED=$?
if [ $GIT_IS_INSTALLED -eq 0 ]; then
	echo [INFO] Git is installed
else
	if [ $LINUX -eq 1 ]; then
	    if [ $DISTRO -eq 1 ]; then
	    	echo [INFO] RHEL detected
			echo [INFO] Installing Git...
			sudo yum install --assumeyes git
		elif [ $DISTRO -eq 2 ]; then
			echo [INFO] Debian detected
			echo [INFO] Updating apt repository...
			sudo apt-get update > /dev/null
			echo [INFO] Installing Git...
			sudo apt-get install --assume-yes --force-yes git > /dev/null
		else
			echo [ERROR] Linux distro $DISTRO is unsupported, aborting
			exit 1
		fi
	elif [ $DARWIN -eq 1 ]; then
		brew --version
    	BREW_IS_INSTALLED=$?
    	if [ $BREW_IS_INSTALLED -eq 0 ]; then
			echo [INFO] Updating brew repository...
			brew update > /dev/null
			echo [INFO] Installing Git...
			brew install git > /dev/null
		else
			echo [ERROR] You must install homebrew - http://brew.sh - before proceeding, aborting
			exit 1
		fi
	else
		echo [ERROR] Platform is unsupported, aborting
		exit 1
	fi
fi

# Clone the provisioning repo

pushd $DEVOPS_PREFIX > /dev/null
if [ -d $CHEF_REPO ]; then
	cd $CHEF_REPO > /dev/null
	sudo git fetch origin > /dev/null
	if [ $? -ne 0 ]; then
		echo [ERROR] There is a problem with $DEVOPS_PREFIX/$CHEF_REPO. Please delete this directory and try again. Aborting.
		exit 1
	fi
	sudo git reset --hard origin/master > /dev/null
	if [ $? -ne 0 ]; then
		echo [ERROR] There is a problem with $DEVOPS_PREFIX/$CHEF_REPO. Please delete this directory and try again. Aborting.
		exit 1
	fi
else
	echo [INFO] Cloning provisioning repo...
	sudo git clone $REMOTE_URL > /dev/null
	if [ $? -ne 0 ]; then
		echo [ERROR] Clone failed, aborting
		exit 1
	fi
	cd $CHEF_REPO > /dev/null
fi

# Install Chef
echo [INFO] Installing chef-client...
sudo ./install-chef.sh > /dev/null
#curl -L https://omnitruck.chef.io/install.sh | sudo bash
if [ $? -ne 0 ]; then
	echo [ERROR] Chef install failed, aborting
	exit 1
fi

echo [INFO] Boostrapping complete!

# Invoke devops script
./devops.sh

popd > /dev/null
