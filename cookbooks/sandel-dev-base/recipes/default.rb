#
# Cookbook Name:: sandel-dev-base
# Recipe:: default
#
# Copyright 2015, Sandel Avionics, Inc.
#
# All rights reserved - Do Not Redistribute
#
# Configures and updates the native package manager, and installs
# any required base packages for development machines.
# 
# Note: Packages to be installed are defined in 'attributes/default.rb'.
# This simply loops through each package and installs it, if necessary.
#

# Clone / Update the DevOps repo:
git node['sandel-dev-base']['devops-home'] do
  repository node['sandel-dev-base']['devops-repo-uri']
  revision 'master'
  action :sync
end

# Update the version file:
file node['sandel-dev-base']['devops-home'] + '/' + node['sandel-dev-base']['devops-version-file'] do
  content <<-EOH
#{run_context.cookbook_collection['sandel-dev-base'].metadata.version}
    EOH
  mode '0755'
  owner 'root'
  group 'root'
end

# Configure and update the native package manager, if necessary:
case node[:platform]
	when 'redhat','centos'
		bash "Download rhel7-epel-rpm-uri" do
		  user "root"
		  group "root"
		  code <<-EOH
		    curl --output /tmp/epel-release-latest-7.noarch.rpm #{node['sandel-dev-base']['rhel7-epel-rpm-uri']}
		    EOH
		  not_if "rpm -qa | grep 'epel-release'"
		end
		rpm_package '/tmp/epel-release-latest-7.noarch.rpm' do
		  action :upgrade
		  not_if "rpm -qa | grep 'epel-release'"
		end
		bash "Remove temp epel RPM file (RHEL)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    rm /tmp/epel-release-latest-7.noarch.rpm
		    EOH
		  only_if "test -f /tmp/epel-release-latest-7.noarch.rpm"
		end
		bash "Enable Optional Yum Repos (RHEL)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    subscription-manager repos --enable=rhel-7-workstation-optional-rpms
		    EOH
		end
		bash "Install Development Tools (RHEL)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    yum groupinstall "Development Tools" --assumeyes
		    EOH
		  not_if "yum grouplist installed | grep 'Development Tools'"
		end
		node['sandel-dev-base']['pkgs-rhel'].each do |pkg|
		  package pkg['name']
		end
    # Update default SVN repo to get latest SVN version (1.9.x)
    file '/etc/yum.repos.d/wandisco-svn.repo' do
      content <<-EOH
[WandiscoSVN]
name=Wandisco SVN Repo
baseurl=http://opensource.wandisco.com/centos/7/svn-1.9/RPMS/$basearch/
enabled=1
gpgcheck=0
      EOH
      mode '0644'
      owner 'root'
      group 'root'
    end
    # Remove old version of SVN if present
    bash "Remove Old SVN" do
      user "root"
      group "root"
      code <<-EOH
        yum remove subversion* --assumeyes
        yum clean all
        yum install subversion --assumeyes
        yum install git-svn --assumeyes
        EOH
      not_if "svn --version | grep 'svn, version 1.9'"
    end
    # Remove old version of CMake if present
    bash "Remove Old CMake" do
      user "root"
      group "root"
      code <<-EOH
        yum remove cmake --assumeyes
        yum clean all
        EOH
      not_if "cmake3 --version | grep 'cmake version 3'"
    end
	when 'debian','ubuntu'
		include_recipe 'apt::default'
		node['sandel-dev-base']['pkgs-debian-ubuntu'].each do |pkg|
		  package pkg['name']
		end
#	when 'os_x'
end

# Install common packages:
node['sandel-dev-base']['pkgs-common'].each do |pkg|
  package pkg['name']
end

# Set the default env variables (RedHat):


# Set the default env variables:
case node[:platform]
  when 'redhat','centos'
    file node['sandel-dev-base']['devops-env-file'] do
      content <<-EOH
export DEVOPS_HOME=#{node['sandel-dev-base']['devops-home']}
export SAGE_HOME=#{node['sandel-dev-base']['sage-edk']}
export P4PORT=#{node['sandel-dev-base']['p4port']}
export VECTORCAST_DIR=#{node['sandel-dev-base']['vectorcast']}
export LM_LICENSE_FILE=#{node['sandel-dev-base']['vectorcast-lic']}
. #{node['sandel-dev-base']['glstudio-sh']}
        EOH
      mode '0755'
      owner 'root'
      group 'root'
    end
  when 'debian','ubuntu'
    file node['sandel-dev-base']['devops-env-file'] do
      content <<-EOH
export DEVOPS_HOME=#{node['sandel-dev-base']['devops-home']}
export SAGE_HOME=#{node['sandel-dev-base']['sage-edk']}
export P4PORT=#{node['sandel-dev-base']['p4port']}
export VECTORCAST_DIR=#{node['sandel-dev-base']['vectorcast']}
export LM_LICENSE_FILE=#{node['sandel-dev-base']['vectorcast-lic']}
        EOH
      mode '0755'
      owner 'root'
      group 'root'
    end
end

# Ubuntu only: Reference /etc/profile.d/devops.sh in /etc/bash.bashrc:
# (So env vars will be set upon opening a new Terminal)
case node[:platform]
	when 'debian','ubuntu'
		bash "Append /etc/bash.bashrc env vars (Ubuntu)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    printf '\n\n\#devops\n. #{node['sandel-dev-base']['devops-env-file']}\n' >> /etc/bash.bashrc
		    EOH
		  not_if "grep '\#devops' /etc/bash.bashrc"
		end
end

# Set VirtualBox yum repo on RHEL:
case node[:platform]
	when 'redhat','centos'
		file node['sandel-dev-base']['vbox-yum-repo'] do
		  content <<-EOH
[virtualbox]
name=Oracle Linux / RHEL / CentOS-$releasever / $basearch - VirtualBox
baseurl=http://download.virtualbox.org/virtualbox/rpm/el/$releasever/$basearch
enabled=1
gpgcheck=1
gpgkey=http://download.virtualbox.org/virtualbox/debian/oracle_vbox.asc
		    EOH
		  mode '0644'
		  owner 'root'
		  owner 'root'
		end
end

# Install VirtualBox on RHEL:
case node[:platform]
	when 'redhat','centos'
		bash "Install VirtualBox (RHEL)" do
		  user "root"
                  group "root"
                  code <<-EOH
		    export KERN_DIR=/usr/src/kernels/`uname -r`
		    yum install VirtualBox-5.0 --assumeyes
		    EOH
		  not_if "yum list installed VirtualBox-5.0"
		end	
end

# Install Sage EDK
bash "Install SageEDK" do
  user "root"
  group "root"
  code <<-EOH
    curl --output /tmp/sage_edk_installer.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/sage_edk_installer.tar.gz
    tar --extract --gunzip --overwrite --file /tmp/sage_edk_installer.tar.gz --directory /tmp
    chmod 0777 /tmp/sage_edk_installer/SageEDK_Linux-x86_64-Install
    /tmp/sage_edk_installer/SageEDK_Linux-x86_64-Install --mode silent --prefix #{node['sandel-dev-base']['sage-edk']}
    mv /tmp/sage_edk_installer/license.key #{node['sandel-dev-base']['sage-edk']}
    mv /tmp/sage_edk_installer/Sage_SmartProbe_And_Users_Guide.pdf #{node['sandel-dev-base']['sage-edk']}
    rm -rf /tmp/sage_*
    EOH
  not_if { ::File.directory?("#{node['sandel-dev-base']['sage-edk']}") }
end

# Install Slickedit:
bash "Install Slickedit" do
  user "root"
  group "root"
  code <<-EOH
    curl --output /tmp/slickedit.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/slickedit.tar.gz
    tar --extract --gunzip --overwrite --file /tmp/slickedit.tar.gz --directory /tmp
    expect -c '
      spawn /tmp/slickedit/v18/se_18000013_linux64/vsinst +accept-eula -post-install -destination-dir #{node['sandel-dev-base']['slickedit']}
      expect {
      	"\\\<ENTER\\\> to continue..." {
      		send "\r"
      		exp_continue
      	}
      	-gl "--More--*" {
      		send "\r"
      		exp_continue
      	}
      	-gl "Do you agree*" {
      		send "yes\r"
      		exp_continue
      	}
      }
      interact return'
      cp /tmp/slickedit/v18/se_1800_linux.lic #{node['sandel-dev-base']['slickedit'] + '/bin/slickedit.lic'}
      rm -rf /tmp/slickedit*
      mv #{node['sandel-dev-base']['slickedit'] + '/bin/gdb'} #{node['sandel-dev-base']['slickedit'] + '/bin/vs-gdb'}
    EOH
  not_if { ::File.directory?("#{node['sandel-dev-base']['slickedit']}") }
end
# Required for Slickedit on RHEL:
case node[:platform]
	when 'redhat','centos'
		bash "Update DBUS Machine ID (RHEL)" do
  		  user "root"
  		  group "root"
  		  code <<-EOH
    	    dbus-uuidgen > /var/lib/dbus/machine-id
    	    EOH
  		  not_if { ::File.exist?("/var/lib/dbus/machine-id") }
  		end
end

# Install P4V
bash "Install P4V" do
  user "root"
  group "root"
  code <<-EOH
    curl --output /tmp/p4v.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/p4v.tar.gz
    tar --extract --gunzip --overwrite --file /tmp/p4v.tar.gz --directory /opt
    rm -rf /tmp/p4v.tar.gz
    EOH
  not_if { ::File.directory?("#{node['sandel-dev-base']['p4v']}") }
end

# Install BeyondCompare
case node[:platform]
	when 'redhat','centos'
		bash "Install BeyondCompare (RedHat)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    curl --output /tmp/bcompare-4.1.2.20720.x86_64.rpm https://s3-us-west-1.amazonaws.com/sandel-dev/bcompare-4.1.2.20720.x86_64.rpm
		    EOH
		  not_if "rpm -qa | grep 'bcompare'"
		end
		rpm_package '/tmp/bcompare-4.1.2.20720.x86_64.rpm' do
		  action :install
		  not_if "rpm -qa | grep 'bcompare'"
		end
	when 'debian','ubuntu'
		bash "Install BeyondCompare (Debian/Ubuntu)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    curl --output /tmp/bcompare-4.1.1.20615_amd64.deb https://s3-us-west-1.amazonaws.com/sandel-dev/bcompare-4.1.1.20615_amd64.deb
		    EOH
		  not_if "dpkg-query -W bcompare"
		end
		dpkg_package '/tmp/bcompare-4.1.1.20615_amd64.deb' do
		  action :install
		  not_if "dpkg-query -W bcompare"
		end
end
# Remove bcompare tmp files:
bash "Remove bcompare tmp files" do
  user "root"
  group "root"
  code <<-EOH
    rm -rf /tmp/bcompare*
    EOH
end

# Install Affinic Debugger
bash "Install ADG" do
  user "root"
  group "root"
  code <<-EOH
    curl --output /tmp/adg_linux.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/adg_linux.tar.gz
    tar --extract --gunzip --overwrite --file /tmp/adg_linux.tar.gz --directory /opt
    rm /tmp/adg_linux.tar.gz
    EOH
  not_if { ::File.directory?("#{node['sandel-dev-base']['adg']}") }
end

# Install Putty CLI Tools
bash "Install Putty" do
  user "root"
  group "root"
  code <<-EOH
    curl --output /tmp/putty-linux-cli.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/putty-linux-cli.tar.gz
    tar --extract --gunzip --overwrite --file /tmp/putty-linux-cli.tar.gz --directory /opt
    rm /tmp/putty-linux-cli.tar.gz
    EOH
  not_if { ::File.directory?("#{node['sandel-dev-base']['putty']}") }
end

# Install i386-elf-gcc Cross Toolchain (xtools):
case node[:platform]
	when 'redhat','centos'
		bash "Install i386-elf-gcc Crosstools (RHEL)" do
  		  user "root"
  		  group "root"
  		  code <<-EOH
    	    curl --output /tmp/xtools-rhel.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/xtools-rhel.tar.gz
    	    mkdir -p #{node['sandel-dev-base']['devops-home'] + '/toolchains'}
    	    tar --extract --gunzip --overwrite --file /tmp/xtools-rhel.tar.gz --directory #{node['sandel-dev-base']['devops-home'] + '/toolchains'}
    	    mv #{node['sandel-dev-base']['devops-home'] + '/toolchains' + '/usr/local/devops/toolchains/xtools'} #{node['sandel-dev-base']['devops-home'] + '/toolchains'}
    	    rm -rf #{node['sandel-dev-base']['devops-home'] + '/toolchains' + '/usr'}
    	    rm -rf /tmp/xtools-rhel.tar.gz
    	    EOH
    	  not_if { ::File.directory?("#{node['sandel-dev-base']['devops-home'] + '/toolchains/xtools'}") }
    	end
	when 'debian','ubuntu'
		bash "Install i386-elf-gcc Crosstools (Debian/Ubuntu)" do
  		  user "root"
  		  group "root"
  		  code <<-EOH
    	    curl --output /tmp/xtools.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/xtools.tar.gz
    	    mkdir -p #{node['sandel-dev-base']['devops-home'] + '/toolchains'}
    	    tar --extract --gunzip --overwrite --file /tmp/xtools.tar.gz --directory #{node['sandel-dev-base']['devops-home'] + '/toolchains'}
    	    rm -rf /tmp/xtools.tar.gz
    	    EOH
    	  not_if { ::File.directory?("#{node['sandel-dev-base']['devops-home'] + '/toolchains/xtools'}") }
    	end
end

# NetExtender (VPN Client)
case node[:platform]
	when 'redhat','centos','debian','ubuntu'
		bash "Install NetExtender (VPN Client)" do
		  user "root"
		  group "root"
		  code <<-EOH
		    curl --output /tmp/netExtenderClient.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/netExtenderClient.tar.gz
		    tar --extract --gunzip --overwrite --file /tmp/netExtenderClient.tar.gz --directory /tmp
		    cd /tmp/netExtenderClient
		    chmod a+x install
		    expect -c '
		      spawn ./install
		      expect {
		      	-gl "*Set pppd to run as root*" {
		      		send "y\r"
		      		exp_continue
		      	}
		      }
		      interact return'
		    rm -rf /tmp/netExtender*
		    EOH
		  not_if "which netExtender"
		end
end

# Vectorcast
case node[:platform]
	when 'redhat','centos','debian','ubuntu'
		bash "Install Vectorcast" do
		  user "root"
		  group "root"
		  code <<-EOH
		    curl --output /tmp/vcast.linux.64b.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/vcast.linux.64b.tar.gz
		    mkdir -p #{node['sandel-dev-base']['vectorcast']}
		    tar --extract --gunzip --overwrite --file /tmp/vcast.linux.64b.tar.gz --directory #{node['sandel-dev-base']['vectorcast']}
		    rm -rf /tmp/vcast*
		    EOH
		  not_if { ::File.directory?("#{node['sandel-dev-base']['vectorcast']}") }
		end
end

# GLStudio
case node[:platform]
  when 'redhat','centos'
    bash "Install GLStudio" do
      user "root"
      group "root"
      code <<-EOH
        mkdir -p /tmp/disti/sce_cpp
        curl --output /tmp/disti/GLStudio_5_0_2002_Linux_Main.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/GLStudio_5_0_2002_Linux_Main.tar.gz
        curl --output /tmp/disti/GLStudio_5_0_2002_Linux64_gcc_480.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/GLStudio_5_0_2002_Linux64_gcc_480.tar.gz
        curl --output /tmp/disti/sce_cpp/GLStudio_SCECpp_1_1_Linux.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/GLStudio_SCECpp_1_1_Linux.tar.gz
        curl --output /tmp/disti/CentOS_Hotfix_ST-172_for_GLS_5.0.2.tar.gz https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/CentOS_Hotfix_ST-172_for_GLS_5.0.2.tar.gz
        tar --extract --gunzip --overwrite --file /tmp/disti/GLStudio_5_0_2002_Linux_Main.tar.gz --directory /tmp/disti
        tar --extract --gunzip --overwrite --file /tmp/disti/GLStudio_5_0_2002_Linux64_gcc_480.tar.gz --directory /tmp/disti
        cd /tmp/disti
        expect -c '
            spawn ./install
            expect {
              -gl "--More--*" {
                  send "\r"
                  exp_continue
              }
              -gl "Do you accept*" {
                  send "Y\r"
                  exp_continue
              }
              -gl "Install Directory*" {
                  send "#{node['sandel-dev-base']['glstudio']}\r"
                  exp_continue
              }
              -gl "(Y/N)*" {
                  send "Y\r"
                  exp_continue
              }
              -gl "Install 64-bit*" {
                  send "Y\r"
                  exp_continue
              }
              -gl "Installation complete*" {
                  send "\r"
                  exp_continue
              }
            }
            interact return'
          cd /tmp/disti/sce_cpp
          tar xzf GLStudio_SCECpp_1_1_Linux.tar.gz
          cd GLStudio_SCECpp
          tar xzf SCECppCodeGenerator_Installation_Linux.tar.gz
          cp SCECppCodeGenerator_Linux.tar SCECppCodeGenerator_Linux64.tar
          expect -c '
            spawn ./install
            expect {
              -gl "Is this OK*" {
                  send "Y\r"
                  exp_continue
              }
            }
            interact return'
          tar --extract --gunzip --overwrite --file /tmp/disti/CentOS_Hotfix_ST-172_for_GLS_5.0.2.tar.gz --directory #{node['sandel-dev-base']['glstudio']}/bin
        cp /root/.glstudio.sh #{node['sandel-dev-base']['glstudio-sh']}
        chown -R root.root #{node['sandel-dev-base']['glstudio']}
        chmod -R a+rx #{node['sandel-dev-base']['glstudio']}
        find #{node['sandel-dev-base']['glstudio']} -type d -exec chmod 755 {} +
        rm -rf /tmp/disti
        EOH
      not_if { ::File.directory?("#{node['sandel-dev-base']['glstudio']}")}
    end
end

# Set the GLStudio launch command `glStudio`:
case node[:platform]
  when 'redhat','centos'
    file node['sandel-dev-base']['glstudio-run'] do
      content <<-EOH
    #!/bin/bash
    # Shortcut to launch glStudio
    QT_PLUGIN_PATH=${GLSTUDIO}/bin LD_LIBRARY_PATH=${GLSTUDIO}/bin:${LD_LIBRARY_PATH} ${GLSTUDIO}/bin/glStudio
        EOH
      mode '0755'
      owner 'root'
      group 'root'
    end
end

# GLStudio - extra lib 'libgls_DO178B_Linux_gcc_480.a'
case node[:platform]
  when 'redhat','centos'
    bash "Install GLStudio Supplemental Library: libgls_DO178B_Linux_gcc_480.a" do
      user "root"
      group "root"
      code <<-EOH
        curl --output #{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libgls_DO178B_Linux_gcc_480.a https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/libgls_DO178B_Linux_gcc_480.a
        chown root.root #{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libgls_DO178B_Linux_gcc_480.a
        chmod a+rx #{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libgls_DO178B_Linux_gcc_480.a
        EOH
      not_if { ::File.exist?("#{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libgls_DO178B_Linux_gcc_480.a") }
    end
end

# GLStudio - extra lib 'libglsd_DO178B_Linux_gcc_480.a'
case node[:platform]
  when 'redhat','centos'
    bash "Install GLStudio Supplemental Library: libglsd_DO178B_Linux_gcc_480.a" do
      user "root"
      group "root"
      code <<-EOH
        curl --output #{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libglsd_DO178B_Linux_gcc_480.a https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/libglsd_DO178B_Linux_gcc_480.a
        chown root.root #{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libglsd_DO178B_Linux_gcc_480.a
        chmod a+rx #{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libglsd_DO178B_Linux_gcc_480.a
        EOH
      not_if { ::File.exist?("#{node['sandel-dev-base']['glstudio']}/SCECpp/lib/libglsd_DO178B_Linux_gcc_480.a") }
    end
end

# GLStudio - Supplemental lib and header updates for the SCEC++ RT for Linux
case node[:platform]
  when 'redhat','centos'
    bash "Install GLStudio Supplemental SCEC++ RT lib for Linux" do
      user "root"
      group "root"
      code <<-EOH
        mkdir -p /tmp/gls_scecpp
        mkdir #{node['sandel-dev-base']['glstudio']}/SCECpp/include/inc_bak
        mv #{node['sandel-dev-base']['glstudio']}/SCECpp/include/*.h #{node['sandel-dev-base']['glstudio']}/SCECpp/include/inc_bak/
        curl --output /tmp/gls_scecpp/libglsscecpp.zip https://s3-us-west-1.amazonaws.com/sandel-dev/glstudio/libglsscecpp.zip
        cd /tmp/gls_scecpp
        unzip libglsscecpp.zip
        cp inc/* #{node['sandel-dev-base']['glstudio']}/SCECpp/include
        EOH
      not_if { ::File.directory?("#{node['sandel-dev-base']['glstudio']}/SCECpp/include/inc_bak") }
    end
end

# Create/Update symlinks:
node['sandel-dev-base']['symlinks-common'].each do |symlink|
  link symlink['from'] do
    to symlink['to']
  end
end
