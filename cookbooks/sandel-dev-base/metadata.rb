name             'sandel-dev-base'
maintainer       'Sandel Avionics, Inc.'
maintainer_email 'bjenkins@sandel.com'
license          'All rights reserved'
description      'Installs/Configures sandel-dev-base'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'

depends 'apt'