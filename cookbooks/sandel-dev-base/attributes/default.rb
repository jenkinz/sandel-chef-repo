# sandel-dev-base attributes/default.rb
#

# Global Constants:
default['sandel-dev-base']['devops-home'] = '/usr/local/devops'
default['sandel-dev-base']['devops-repo-uri'] = 'https://jenkinz@bitbucket.org/jenkinz/devops.git'
default['sandel-dev-base']['devops-cmd'] = '/usr/local/bin/devops'
default['sandel-dev-base']['chef-repo'] = '/usr/local/sandel-chef-repo'
default['sandel-dev-base']['devops-env-file'] = '/etc/profile.d/devops-env.sh'
default['sandel-dev-base']['devops-version-file'] = 'devops-version'
default['sandel-dev-base']['glstudio'] = '/opt/glstudio'
default['sandel-dev-base']['glstudio-sh'] = '/opt/glstudio/glstudio.sh'
default['sandel-dev-base']['glstudio-run'] = '/opt/glstudio/glstudio-run.sh'
default['sandel-dev-base']['p4port'] = 'ssl:dev.sandel.local:1666'
default['sandel-dev-base']['sage-edk'] = '/opt/sage_edk'
default['sandel-dev-base']['slickedit'] = '/opt/slickedit'
default['sandel-dev-base']['p4v'] = '/opt/p4v'
default['sandel-dev-base']['adg'] = '/opt/adg'
default['sandel-dev-base']['putty'] = '/opt/putty-linux-cli'
default['sandel-dev-base']['vectorcast'] = '/opt/vectorcast'
default['sandel-dev-base']['vectorcast-lic'] = '27101@LIC-001'
default['sandel-dev-base']['vbox-yum-repo'] = '/etc/yum.repos.d/virtualbox.repo'
default['sandel-dev-base']['rhel7-epel-rpm-uri'] = 'https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm'

# Base packages required for sandel-dev machines:
default['sandel-dev-base']['pkgs-common'] = [
  {
        :name => 'dejagnu'
  },
  {
        :name => 'dos2unix'
  },
  {
        :name => 'expect'
  },
  {
        :name => 'gawk'
  },
  {
        :name => 'subversion'
  },
  {
        :name => 'git'
  },
  {
        :name => 'git-svn'
  },
  {
        :name => 'graphviz'
  },
  {
        :name => 'm4'
  },
  {
        :name => 'minicom'
  },
  {
        :name => 'nmap'
  },
  {
        :name => 'nasm'
  },
  {
        :name => 'openssh-server'
  },
  {
        :name => 'python'
  },
  {
        :name => 'texinfo'
  },
  {
        :name => 'tcl'
  },
  {
        :name => 'tcpdump'
  },
  {
        :name => 'tree'
  },
  {
        :name => 'vim'
  },
  {
        :name => 'wine'
  },
  {
        :name => 'xclip'
  }
]

# Additional base packages required for RHEL sandel-dev machines:
default['sandel-dev-base']['pkgs-rhel'] = [
  {
        :name => 'adwaita-gtk2-theme.i686'
  },
  {
        :name => 'audiofile.i686'
  },
  {
        :name => 'audiofile-devel'
  },
  {
        :name => 'cmake3'
  },
  {
        :name => 'dkms'
  },
  {
        :name => 'freeglut'
  },
  {
        :name => 'freeglut-devel'
  },
  {
        :name => 'freeglut.i686'
  },
  {
        :name => 'freeglut-devel.i686'
  },
  {
        :name => 'freetype-devel'
  },
  {
        :name => 'gdk-pixbuf2.i686'
  },
  {
        :name => 'glibc.i686'
  },
  {
        :name => 'glibc-devel.i686'
  },
  {
        :name => 'glib2.i686'
  },
  {
        :name => 'gtk2.i686'
  },
  {
        :name => 'gtk2-devel'
  },
  {
        :name => 'libcanberra-gtk2.i686'
  },
  {
        :name => 'mesa-libGL-devel'
  },
  {
        :name => 'mesa-libEGL'
  },
  {
        :name => 'mesa-libEGL.i686'
  },
  {
        :name => 'mesa-libGLU'
  },
  {
        :name => 'mesa-libGLU.i686'
  },
  {
        :name => 'mesa-libGLU-devel'
  },
  {
        :name => 'libgomp'
  },
  {
        :name => 'libstdc++.i686'
  },
  {
        :name => 'libstdc++-devel.i686'
  },
  {
        :name => 'libpng12' # Required for ADG
  },
  {
        :name => 'libusb-devel'
  },
  {
        :name => 'libX11-devel' # Required for wine
  },
  {
        :name => 'libXrender.i686'
  },
  {
        :name => 'libXi.i686'
  },
  {
        :name => 'libSM.i686'
  },
  {
        :name => 'fontconfig.i686'
  },
  {
        :name => 'mesa-dri-drivers.i686' # Required for GLStudio
  },
  {
        :name => 'ncurses'
  },
  {
        :name => 'ncurses-devel'
  },
  {
        :name => 'ntfs-3g'
  },
  {
        :name => 'openssh-clients'
  },
  {
        :name => 'PackageKit-gtk3-module.i686'
  },
  {
        :name => 'python-devel'
  },
  {
        :name => 'qt'
  },
  {
        :name => 'qt-x11'
  },
  {
        :name => 'samba'
  },
  {
        :name => 'telnet'
  }
]

# Additional base packages required for Debian/Ubuntu Linux sandel-dev machines:
default['sandel-dev-base']['pkgs-debian-ubuntu'] = [
  {
        :name => 'automake'
  },
  {
        :name => 'autoconf'
  },
  {
        :name => 'bison'
  },
  {
        :name => 'build-essential'
  },
  {
        :name => 'cmake'
  },
  {
        :name => 'curl'
  },
  {
        :name => 'doxygen'
  },
  {
        :name => 'flex'
  },
  {
        :name => 'freeglut3'
  },
  {
        :name => 'freeglut3:i386'
  },
  {
        :name => 'freeglut3-dev'
  },
  {
        :name => 'freeglut3-dev:i386'
  },
  {
        :name => 'gcc-multilib'
  },
  {
        :name => 'g++-multilib'
  },
  {
        :name => 'lib32stdc++6'
  },
  {
        :name => 'libtool'
  },
  {
        :name => 'openssh-client'
  },
  {
        :name => 'python-dev'
  },
  {
        :name => 'telnetd'
  }
]

# Symlinks to create:
default['sandel-dev-base']['symlinks-common'] = [
  {
        :from => '/usr/local/bin/devops',
        :to => '/usr/local/sandel-chef-repo/devops.sh'
  },
  {
        :from => '/usr/local/bin/cmake',
        :to => '/usr/bin/cmake3'
  },
  {
        :from => '/usr/local/bin/mk',
        :to => '/usr/local/devops/opus/bins/mk'
  },
  {
        :from => '/usr/local/bin/mkmf',
        :to => '/usr/local/devops/opus/bins/mkmf'
  },
  {
        :from => '/usr/local/bin/omake',
        :to => '/usr/local/devops/opus/bins/omake'
  },
  {
        :from => '/usr/local/bin/check',
        :to => '/usr/local/devops/codecheck/bin/1500b1/check'
  },
  {
        :from => '/usr/local/bin/mkccp',
        :to => '/usr/local/devops/codecheck/bin/1500b1/mkccp/mkccp'
  },
  {
        :from => '/usr/local/bin/flint',
        :to => '/usr/local/devops/flint/flint'
  },
  {
        :from => '/usr/local/bin/elapsedtime',
        :to => '/usr/local/devops/internal/elapsedtime/bin/elapsedtime'
  },
  {
        :from => '/usr/local/bin/filedup',
        :to => '/usr/local/devops/internal/filedup/bin/filedup'
  },
  {
        :from => '/usr/local/bin/glintlookup',
        :to => '/usr/local/devops/internal/glintlookup/bin/glintlookup'
  },
  {
        :from => '/usr/local/bin/glintmetric',
        :to => '/usr/local/devops/internal/glintmetric/bin/glintmetric'
  },
  {
        :from => '/usr/local/bin/hexdumpf',
        :to => '/usr/local/devops/internal/hexdumpf/bin/hexdumpf'
  },
  {
        :from => '/usr/local/bin/chksump1',
        :to => '/usr/local/devops/internal/chksump1/bin/chksump1'
  },
  {
        :from => '/usr/local/bin/lint2cs',
        :to => '/usr/local/devops/internal/lint2cs/bin/lint2cs'
  },
  {
        :from => '/usr/local/bin/listfinddup',
        :to => '/usr/local/devops/internal/listfinddup/bin/listfinddup'
  },
  {
        :from => '/usr/local/bin/nice-c',
        :to => '/usr/local/devops/internal/nice-c/bin/nice-c'
  },
  {
        :from => '/usr/local/bin/testreadwritable',
        :to => '/usr/local/devops/internal/testreadwritable/bin/testreadwritable'
  },
  {
        :from => '/usr/local/bin/xfilelookup',
        :to => '/usr/local/devops/internal/xfilelookup/bin/xfilelookup'
  },
  {
        :from => '/usr/local/bin/npath',
        :to => '/usr/local/devops/opensrc/npath/bin/npath'
  },
  {
        :from => '/usr/local/bin/pmccabe',
        :to => '/usr/local/devops/opensrc/pmccabe/bin/pmccabe'
  },
  {
        :from => '/usr/local/bin/vs',
        :to => '/opt/slickedit/bin/vs'
  },
  {
        :from => '/usr/local/bin/vsdiff',
        :to => '/opt/slickedit/bin/vsdiff'
  },
  {
        :from => '/usr/local/bin/vsmerge',
        :to => '/opt/slickedit/bin/vsmerge'
  },
  {
        :from => '/usr/local/bin/p4v',
        :to => '/opt/p4v/p4v-2014.3.1007540/bin/p4v'
  },
  {
        :from => '/usr/local/bin/p4admin',
        :to => '/opt/p4v/p4v-2014.3.1007540/bin/p4admin'
  },
  {
        :from => '/usr/local/bin/p4merge',
        :to => '/opt/p4v/p4v-2014.3.1007540/bin/p4merge'
  },
  {
        :from => '/usr/local/bin/adg',
        :to => '/opt/adg/adg'
  },
  {
        :from => '/usr/local/bin/plink',
        :to => '/opt/putty-linux-cli/plink'
  },
  {
        :from => '/usr/local/bin/pscp',
        :to => '/opt/putty-linux-cli/pscp'
  },
  {
        :from => '/usr/local/bin/psftp',
        :to => '/opt/putty-linux-cli/psftp'
  },
  {
        :from => '/usr/local/bin/pterm',
        :to => '/opt/putty-linux-cli/pterm'
  },
  {
        :from => '/usr/local/bin/putty',
        :to => '/opt/putty-linux-cli/putty'
  },
  {
        :from => '/usr/local/bin/puttygen',
        :to => '/opt/putty-linux-cli/puttygen'
  },
  {
        :from => '/usr/local/bin/puttytel',
        :to => '/opt/putty-linux-cli/puttytel'
  },
  {
        :from => '/usr/local/bin/i386-elf-addr2line',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-addr2line'
  },
  {
        :from => '/usr/local/bin/i386-elf-ar',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-ar'
  },
  {
        :from => '/usr/local/bin/i386-elf-as',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-as'
  },
  {
        :from => '/usr/local/bin/i386-elf-c++',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-c++'
  },
  {
        :from => '/usr/local/bin/i386-elf-c++filt',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-c++filt'
  },
  {
        :from => '/usr/local/bin/i386-elf-cpp',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-cpp'
  },
  {
        :from => '/usr/local/bin/i386-elf-elfedit',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-elfedit'
  },
  {
        :from => '/usr/local/bin/i386-elf-g++',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-g++'
  },
  {
        :from => '/usr/local/bin/i386-elf-gcc',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gcc'
  },
  {
        :from => '/usr/local/bin/i386-elf-gcc-4.8.4',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gcc-4.8.4'
  },
  {
        :from => '/usr/local/bin/i386-elf-gcc-ar',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gcc-ar'
  },
  {
        :from => '/usr/local/bin/i386-elf-gcc-nm',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gcc-nm'
  },
  {
        :from => '/usr/local/bin/i386-elf-gcc-ranlib',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gcc-ranlib'
  },
  {
        :from => '/usr/local/bin/i386-elf-gcov',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gcov'
  },
  {
        :from => '/usr/local/bin/i386-elf-gprof',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-gprof'
  },
  {
        :from => '/usr/local/bin/i386-elf-ld',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-ld'
  },
  {
        :from => '/usr/local/bin/i386-elf-ld.bfd',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-ld.bfd'
  },
  {
        :from => '/usr/local/bin/i386-elf-nm',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-nm'
  },
  {
        :from => '/usr/local/bin/i386-elf-objcopy',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-objcopy'
  },
  {
        :from => '/usr/local/bin/i386-elf-objdump',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-objdump'
  },
  {
        :from => '/usr/local/bin/i386-elf-ranlib',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-ranlib'
  },
  {
        :from => '/usr/local/bin/i386-elf-readelf',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-readelf'
  },
  {
        :from => '/usr/local/bin/i386-elf-size',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-size'
  },
  {
        :from => '/usr/local/bin/i386-elf-strings',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-strings'
  },
  {
        :from => '/usr/local/bin/i386-elf-strip',
        :to => '/usr/local/devops/toolchains/xtools/xtools_output/bin/i386-elf-strip'
  },
  {
        :from => '/usr/lib/libGL.so',
        :to => '/usr/lib/libGL.so.1.2.0'
  },
  {
        :from => '/usr/lib/libGLU.so',
        :to => '/usr/lib/libGLU.so.1'
  },
  {
        :from => '/usr/local/bin/glStudio',
        :to => '/opt/glstudio/glstudio-run.sh'
  },
  {
        :from => '/usr/local/bin/maint',
        :to => '/opt/vectorcast/maint'
  },
  {
        :from => '/usr/local/bin/manage',
        :to => '/opt/vectorcast/manage'
  },
  {
        :from => '/usr/local/bin/vcast_diag',
        :to => '/opt/vectorcast/vcast_diag'
  },
  {
        :from => '/usr/local/bin/vcast_lint',
        :to => '/opt/vectorcast/vcast_lint'
  },
  {
        :from => '/usr/local/bin/vcastqt',
        :to => '/opt/vectorcast/vcastqt'
  },
  {
        :from => '/usr/local/bin/vccover',
        :to => '/opt/vectorcast/vccover'
  },
  {
        :from => '/usr/local/bin/vcd2xml',
        :to => '/opt/vectorcast/vcd2xml'
  },
  {
        :from => '/usr/local/bin/vcdash',
        :to => '/opt/vectorcast/vcdash'
  },
  {
        :from => '/usr/local/bin/vcdb',
        :to => '/opt/vectorcast/vcdb'
  },
  {
        :from => '/usr/local/bin/vcshell',
        :to => '/opt/vectorcast/vcshell'
  },
  {
        :from => '/usr/local/bin/vpython',
        :to => '/opt/vectorcast/vpython'
  }
]
