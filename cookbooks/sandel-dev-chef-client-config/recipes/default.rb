#
# Cookbook Name:: sandel-dev-chef-client-config
# Recipe:: default
#
# Copyright 2015, Sandel Avionics, Inc.
#
# All rights reserved - Do Not Redistribute
#

# Note: depends on 'cron' cookbook:
# 'knife cookbook site install cron'

# Setup hourly cron job to automatically run chef-client:
cron_d 'chef-client-hourly' do
  minute 0
  hour '*'
  command 'chef-client'
  user 'root'
end
