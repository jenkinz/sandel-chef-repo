name             'sandel-dev-chef-client-config'
maintainer       'Sandel Avionics, Inc.'
maintainer_email 'bjenkins@sandel.com'
license          'All rights reserved'
description      'Configures chef-client including cron automation on node machines'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends          'cron'
