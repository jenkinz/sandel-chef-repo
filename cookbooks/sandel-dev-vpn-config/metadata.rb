name             'sandel-dev-vpn-config'
maintainer       'Sandel Avionics, Inc.'
maintainer_email 'bjenkins@sandel.com'
license          'All rights reserved'
description      'Installs/Configures prerequisites for VPN access'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
depends          'hostsfile' # To manage /etc/hosts (or OS equivalent)
