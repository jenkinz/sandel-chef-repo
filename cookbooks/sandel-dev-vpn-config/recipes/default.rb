#
# Cookbook Name:: sandel-dev-vpn-config
# Recipe:: default
#
# Copyright 2015, Sandel Avionics, Inc.
#
# All rights reserved - Do Not Redistribute
#

# Depends on 'hostfile' cookbook. See metadata.rb.
# 'knife cookbook site install hostsfile' must be run on chef workstation
# prior to developing.

hostsfile_entry '10.0.1.50' do
    action :remove
end

# Create each host entry. The host entries are defined in 'attributes/default.rb'
# within this cookbook.
node['sandel-dev-vpn-config']['hosts'].each do |host|
  hostsfile_entry host['ip'] do
    hostname host['name']
    comment host['note']
    action :append
  end
end