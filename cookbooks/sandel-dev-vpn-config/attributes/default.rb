# attributes/default.rb
#

# Sandel Avionics VPN hosts
default['sandel-dev-vpn-config']['hosts'] = [
  {
    :ip => '10.0.1.55',
    :name => 'dev.sandel.local',
    :note => 'Sandel Avionics VPN: Git server'
  },
  {
    :ip => '10.0.1.50',
    :name => 'ci.sandel.local',
    :note => 'Sandel Avionics VPN: Continuous Integration server'
  },
  {
    :ip => '10.0.1.50',
    :name => 'data.sandel.local',
    :note => 'Sandel Avionics VPN: Database server'
  },
  {
    :ip => '10.0.0.203',
    :name => 'cicero.sandel.local',
    :note => 'Sandel Avionics VPN: Cicero (graphicdev) SAMBA share'
  },
  {
    :ip => '10.0.0.250',
    :name => 'subversion.sandel.local',
    :note => 'Sandel Avionics VPN: Subversion server'
  },
  {
    :ip => '10.0.0.207',
    :name => 'hera.sandel.local',
    :note => 'Sandel Avionics VPN: Trac server'
  },
  {
    :ip => '10.0.0.195',
    :name => 'redmine.sandel.local',
    :note => 'Sandel Avionics VPN: Redmine server'
  }
]
