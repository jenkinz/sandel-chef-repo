# attributes/default.rb
#

# The following are the required packages for development builds:
default['sandel-dev-bld-pkgs']['pkgs'] = [
  {
    :name => 'curl'
  },
  {
    :name => 'git'
  },
  {
    :name => 'build-essential'
  }
]
