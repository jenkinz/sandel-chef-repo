sandel-dev-bld-pkgs CHANGELOG
=============================

This file is used to list changes made in each version of the sandel-dev-bld-pkgs cookbook.

0.1.0
-----
- Brian Jenkins  - Initial release of sandel-dev-bld-pkgs

- - -
Check the [Markdown Syntax Guide](http://daringfireball.net/projects/markdown/syntax) for help with Markdown.

The [Github Flavored Markdown page](http://github.github.com/github-flavored-markdown/) describes the differences between markdown on github and standard markdown.
