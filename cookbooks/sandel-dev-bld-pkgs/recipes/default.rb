#
# Cookbook Name:: sandel-dev-bld-pkgs
# Recipe:: default
#
# Copyright 2015, Sandel Avionics, Inc.
#
# All rights reserved - Do Not Redistribute
#

# Note: Packages to be installed are defined in 'attributes/default.rb'.
# This simply loops through each package and installs it, if necessary.
#
# Note: 'apt-get update' should be run as a precursor to this recipe.
# For the 'sandel-dev-machine-internal' role, this is already ensured.
node['sandel-dev-bld-pkgs']['pkgs'].each do |pkg|
  package pkg['name']
end
