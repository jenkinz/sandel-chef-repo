name             'sandel-dev-bld-pkgs'
maintainer       'Sandel Avionics, Inc.'
maintainer_email 'bjenkins@sandel.com'
license          'All rights reserved'
description      'Installs/Configures basic packages required to perform software builds'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.0'
