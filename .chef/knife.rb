# See http://docs.chef.io/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "jenkinz"
client_key               "#{current_dir}/jenkinz.pem"
validation_client_name   "sandel-validator"
validation_key           "#{current_dir}/sandel-validator.pem"
chef_server_url          "https://api.chef.io/organizations/sandel"
cookbook_path            ["#{current_dir}/../cookbooks"]
cookbook_email           'bjenkins@sandel.com'
cookbook_copyright       'Sandel Avionics, Inc.'
