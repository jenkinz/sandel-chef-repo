#!/bin/bash

# DevOps - *local* provisioning updating for sandel-dev machines
# (using chef-zero instead of Chef Server)
#
# Invoke this script to update the current machine's provisioning.
#

DEVOPS_PREFIX=/usr/local
DEVOPS_DIR=devops
CHEF_REPO=sandel-chef-repo
REMOTE_URL=https://jenkinz@bitbucket.org/jenkinz/$CHEF_REPO.git

pushd $DEVOPS_PREFIX > /dev/null
if [ -d $CHEF_REPO ]; then
	cd $CHEF_REPO > /dev/null
	sudo git fetch origin > /dev/null
	if [ $? -ne 0 ]; then
		echo [ERROR] Could not update the Sandel Chef repo at $DEVOPS_PREFIX/$CHEF_REPO. Aborting.
		exit 1
	fi
	sudo git reset --hard origin/master > /dev/null
	if [ $? -ne 0 ]; then
		echo [ERROR] There is a problem with $DEVOPS_PREFIX/$CHEF_REPO. Please delete this directory and try again. Aborting.
		exit 1
	fi
else
	echo [ERROR] DevOps tools have been removed or altered and must be reinstalled. Aborting.
	exit 1
fi
popd > /dev/null

# Invoke Chef Client (local mode, chef-zero)
pushd $DEVOPS_PREFIX/$CHEF_REPO > /dev/null
sudo chef-client --local-mode -o sandel-dev-base,sandel-dev-vpn-config
popd > /dev/null