name "sandel-dev-machine-internal"
description "A role applicable to any internal Sandel development machine. This includes developer workstations and development servers owned and operated by Sandel employees and contractors that have access to the internal Sandel network."
run_list "recipe[apt]", "recipe[sandel-dev-chef-client-config]", "recipe[sandel-dev-bld-pkgs]", "recipe[sandel-dev-vpn-config]"
