name "sandel-dev-machine-local"
description "Local installer/updater for internal Sandel development machines."
run_list "recipe[sandel-dev-base]", "recipe[sandel-dev-vpn-config]"
